:reset-sectlevel: 2+
include::{dir-directive}/section-reset.adoc[]

Berikut beberapa simbol dan istilah yang digunakan dalam dokumentasi ini.

[cols="2,2,3"]
.Penamaan dan Simbol
|===
|Penamaan |Simbol/Nilai |Penjelasan

|`string`
|`""` atau `''`
|Representasi dari kumpulan karakter/huruf/simbol.

|`number`
|`"1"`, `"-1"`, `"1.0"`, `'1'`, `'-1'`, `'1.0'`,
|Representasi dari kumpulan angka  baik dari bilangan bulat atau desimal, dalam bentuk `string`.

|`uuid`
|`uuid`, `uuid1`, `uuid2`, `uuid3`, `uuid4`
|Merepresentasikan ID universal unik dari suatu entitas, yang terdiri dari 36 karakter (32 karakter heksadesimal dan 4 tanda minus), lebih lengkapnya silakan lihat di https://en.wikipedia.org/wiki/Universally_unique_identifier. Contoh data `uuid`: `123e4567-e89b-12d3-a456-426614174000`

|===
