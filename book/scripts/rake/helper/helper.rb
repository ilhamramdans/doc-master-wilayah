require "rbconfig"
require_relative 'git.rb'
require_relative 'readme-file.rb'
require_relative 'vsc-code-snippet.rb'
require_relative 'asciidoctor-document.rb'
require_relative 'asciidoctor-pdf-command.rb'

module Helper
  # output name related
  def self.pdf_output(output_path, output_filename)
    return output_path + '[TBD] ' + output_filename + '.pdf'
  end

  def self.pdf_output_versioned(is_prod, output_path, output_filename)
    return output_path + (is_prod ? '' : '[WIP] ') + output_filename + '.pdf'
  end

  # check os type
  def self.is_os_windows
    return RbConfig::CONFIG['host_os'] =~ /mswin|windows|mingw/i
  end

  # others
  def self.cli_clear
    system(self.is_os_windows ? 'cls' : 'clear')
  end

  def self.read_attr
    if !File.file?(file_name)
      raise "File '#{file_name}' not found!"
    end

    @file_name = file_name

    file = File.open(file_name)
    @lines = file.readlines
    file.close    
  end
end
