ifdef::confidentialy-level[]
ifeval::["{confidentialy-level}" != "none"]

// uncomment this if need watermark, and watermark images are available
//:page-foreground-image: image:confidentialy/{confidentialy-level}.png[]

:subject-entities: {entity-pusdatin} – {entity-dto} ({entity-dto-alt}) {entity-kemkes}
:subject-dto: pass:q,a[*{subject-entities}*]
:subject-provider: pass:q,a[*{entity-pusdatin} - {entity-dto} ({entity-dto-alt}) {entity-kemkes}*]

== Kerahasiaan Informasi

include::{confidentialy-level}.adoc[]

endif::[]
endif::confidentialy-level[]
