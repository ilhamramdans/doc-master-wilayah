require_relative 'helper/helper.rb'

class SyncReadme
  def self.clear_screen
    Helper.cli_clear
    puts "[ Synchronize README.md File ]\n\n"
  end

  def self.sync_content(is_git_ready, file_name = 'README.md')
    readme = Helper::ReadmeFile.new(file_name)
    document = Helper::AsciidoctorDocument.new('book.adoc')

    puts "Synchronize Title\n"

    document_title = "#{document.get_title}, #{document.get_subtitle}"
    show_state(readme.sync_title(document_title))

    if (is_git_ready)
      puts "Synchronize Git Address\n"

      git_remote = Helper::Git.remote
      show_state(readme.sync_git_remote(git_remote))
    end

    puts "Update Content\n"
    readme.update
  end

  private

  def self.show_state(result)
    old_string, new_string, state = result

    puts "  Old: #{old_string}\n"
    puts "  New: #{new_string}\n"

    case state
    when Helper::ReadmeFile::STATUS_CHANGED
      puts "  Status: CHANGED\n"
    when Helper::ReadmeFile::STATUS_UNCHANGED
      puts "  Status: UNCHANGED\n"
    when Helper::ReadmeFile::STATUS_NOT_FOUND
      puts "  Status: NOT FOUND\n"
    else
      puts "  Status: UNKNOWN\n"
    end
  end
end
