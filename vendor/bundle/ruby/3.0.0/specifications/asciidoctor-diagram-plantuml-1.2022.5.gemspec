# -*- encoding: utf-8 -*-
# stub: asciidoctor-diagram-plantuml 1.2022.5 ruby lib

Gem::Specification.new do |s|
  s.name = "asciidoctor-diagram-plantuml".freeze
  s.version = "1.2022.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Pepijn Van Eeckhoudt".freeze]
  s.date = "2022-05-08"
  s.description = "PlantUML JAR files wrapped in a Ruby gem".freeze
  s.email = ["pepijn@vaneeckhoudt.net".freeze]
  s.homepage = "https://github.com/asciidoctor/asciidoctor-diagram".freeze
  s.licenses = ["Apache-2.0".freeze]
  s.rubygems_version = "3.2.3".freeze
  s.summary = "PlantUML JAR files wrapped in a Ruby gem".freeze

  s.installed_by_version = "3.2.3" if s.respond_to? :installed_by_version
end
