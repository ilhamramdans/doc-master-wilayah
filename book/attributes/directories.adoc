:imagesdir: resources/image
:imagesoutdir: resources/image
:dir-book: {docdir}/book
:dir-chapter: {docdir}/chapters
:dir-snippet: {docdir}/snippets
:dir-resource: {docdir}/resources
:dir-directive: {dir-book}/directives
:res-image: {imagesdir}
:res-api: {dir-resource}/api
:res-sql: {dir-resource}/sql
:res-puml: {dir-resource}/plantuml
